# Antialias Filter Design Based on Minicircuits RLP/RHP/RBP Components

## Features

- High rejection
- Sharp insertion loss rolloff
- Good return loss characteristics

## License

[CERN-OHL-W v2.](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2)
